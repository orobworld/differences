# Differences #

### What is this repository for? ###

This code shows how to use an integer programming model and CPLEX's `populate` method to find all lists of integers, of minimum length and with least element 1, whose pairwise differences (after removal of duplicates) match a given list of positive integers.

The problem and model are explained in a [blog post](https://orinanobworld.blogspot.com/2020/02/reversing-differences.html) by me. The [original question](https://or.stackexchange.com/questions/3349/restoring-a-list-from-differences)  is posted on OR Stack Exchange, along with answers regarding the use of constraint programming.

The code was developed using CPLEX 12.10 but will run with at least some earlier versions.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

