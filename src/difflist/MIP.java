package difflist;

import ilog.concert.IloException;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * MIP provides a MIP model for the difference problem.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MIP implements AutoCloseable {
  private final IloCplex mip;                        // the MIP model
  private final HashMap<Integer, IloNumVar> varMap;  // maps integer to variable
  private final HashMap<IloNumVar, Integer> intMap;  // maps variable to integer
  private final HashMap<Pair, IloNumVar> pairMap;    // maps pairs to indicators
  private final ArrayList<IloNumVar> allVars;        // all variables

  /**
   * Constructor.
   * @param problem the underlying problem
   * @throws IloException if the model cannot be built
   */
  public MIP(final Problem problem) throws IloException {
    // Set up the model.
    mip = new IloCplex();
    varMap = new HashMap<>();
    intMap = new HashMap<>();
    pairMap = new HashMap<>();
    // Create the binary variables for integer inclusion.
    allVars = new ArrayList<>();
    int lb = problem.getLb();
    int ub = problem.getUb();
    for (int i = lb; i <= ub; i++) {
      IloNumVar v = mip.boolVar("Use_" + i);
      varMap.put(i, v);
      intMap.put(v, i);
      allVars.add(v);
      // The lowest value is automatically included.
      if (i == lb) {
        v.setLB(1);
      }
    }
    // The objective is to minimize the number of values used.
    mip.addMinimize(mip.sum(allVars.toArray(new IloNumVar[allVars.size()])));
    // Now add indicator variables for all possible pairs, along with
    // constraints defining them.
    for (Pair p : problem.getAllPairs()) {
      IloNumVar v = mip.numVar(0, 1, p.toString());
      pairMap.put(p, v);
      allVars.add(v);
      int i = p.getFirst();
      int j = p.getSecond();
      IloNumVar xi = varMap.get(i);
      IloNumVar xj = varMap.get(j);
      mip.addGe(mip.diff(v, mip.sum(xi, xj)), -1);
      mip.addLe(v, xi);
      mip.addLe(v, xj);
      // Prohibited pairs of integers cannot be allowed.
      if (problem.isTabu(p)) {
        v.setUB(0);
      }
    }
    // For differences in the difference list, at least one pair of values
    // that cover them must be included.
    for (int d : problem.getDiffs()) {
      IloNumVar[] v =
        problem.getCover(d)
               .stream()
               .map((p) -> pairMap.get(p))
               .collect(Collectors.toList())
               .toArray(new IloNumVar[1]);
      mip.addGe(mip.sum(v), 1, "MakeDiff_" + d);
    }
  }

  /**
   * Close the model.
   * @throws Exception if anything goes splat.
   */
  @Override
  public void close() throws Exception {
    mip.end();
  }

  /**
   * Runs the MIP model.
   * @return the final model status
   * @throws IloException if CPLEX blows up
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public IloCplex.Status solve() throws IloException {
    // Set parameters for the solution pool.
    mip.setParam(IloCplex.Param.MIP.Pool.AbsGap, 0.5);
      // eliminates suboptimal solutions
    mip.setParam(IloCplex.Param.MIP.Limits.Populate, 20);
      // stop at 20 solutions
    mip.setParam(IloCplex.Param.MIP.Pool.Replace, 1);
      // replace worst objective value -- probably redundant
    mip.setParam(IloCplex.Param.MIP.Pool.Intensity, 3);
      // search aggressively
    long start = System.currentTimeMillis();
    mip.populate();
    long time = System.currentTimeMillis() - start;
    System.out.println("\nThe solver ran for " + time + " ms.");
    return mip.getStatus();
  }

  /**
   * Gets the number of solutions in the pool.
   * @return the pool solution count
   * @throws IloException if CPLEX balks
   */
  public int getNSolutions() throws IloException {
    return mip.getSolnPoolNsolns();
  }

  /**
   * Gets a solution.
   * @param n the index of the solution to retrieve
   * @return the list of integers comprising the solution
   * @throws IloException if the solution cannot be recovered
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public List<Integer> getSolution(final int n) throws IloException {
    ArrayList<Integer> used = new ArrayList<>();
    IloNumVar[] vars = varMap.values().toArray(new IloNumVar[varMap.size()]);
    double[] vals = mip.getValues(vars, n);
    for (int i = 0; i < vals.length; i++) {
      if (vals[i] > 0.5) {
        used.add(intMap.get(vars[i]));
      }
    }
    Collections.sort(used);
    return used;
  }

  /**
   * Returns the final objective value.
   * @return the objective value
   * @throws IloException if there is no objective value
   */
  public int getObjValue() throws IloException {
    return (int) Math.round(mip.getObjValue());
  }
}
