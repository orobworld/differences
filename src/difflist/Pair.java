package difflist;

/**
 * Pair encapsulates a pair of integer within the domain of the solution.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Pair {
  private final int lesser;      // the smaller integer
  private final int greater;     // the larger integer
  private final int diff;        // the absolute difference

  /**
   * Constructor.
   * @param a the first integer in the pair
   * @param b the second integer in the pair
   */
  public Pair(final int a, final int b) {
    lesser = Math.min(a, b);
    greater = Math.max(a, b);
    diff = b - a;
  }

  /**
   * Gets the absolute difference.
   * @return the absolute difference
   */
  public int getDiff() {
    return diff;
  }

  /**
   * Gets the smaller member of the pair.
   * @return the smaller member
   */
  public int getFirst() {
    return lesser;
  }

  /**
   * Gets the larger member of the pair.
   * @return the larger member
   */
  public int getSecond() {
    return greater;
  }

  /**
   * Gets a string describing the pair.
   * @return a description of the pair
   */
  @Override
  public String toString() {
    return "Pair_" + lesser + "_" + greater;
  }
}
