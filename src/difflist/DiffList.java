package difflist;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;

/**
 * DiffList tries to solve the problem of reconstructing an integer list from
 * the list of all pairwise differences in absolute value (with duplicates
 * removed).
 * See
 * https://or.stackexchange.com/questions/3349/restoring-a-list-from-differences
 * for details.
 *
 * The input data (list of differences) is hard-coded in the main method.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class DiffList {

  /**
   * Dummy constructor.
   */
  private DiffList() { }

  /**
   * Tries to solve the problem using a MIP model.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Create a problem instance.
    Problem problem =
      new Problem(new int[]{1, 6, 7, 8, 12, 13, 14, 15, 19, 20,
                            21, 26, 27, 33, 34});
    // Create and solve a MIP model.
    try (MIP mip = new MIP(problem)) {
      IloCplex.Status status = mip.solve();
      System.out.println("Final MIP status = " + status);
      if (status == IloCplex.Status.Optimal
          || status == IloCplex.Status.Feasible) {
        int nSol = mip.getNSolutions();
        System.out.println("\nThe solution pool contains "
                           + nSol + " solutions.");
        System.out.println("\nThe optimal list uses " + mip.getObjValue()
                           + " integers.");
        System.out.println("Optimal solutions:");
        for (int i = 0; i < nSol; i++) {
          System.out.println(mip.getSolution(i).toString());
        }
      }
    } catch (IloException ex) {
      System.err.println("CPLEX threw an exception:\n" + ex.getMessage());
    } catch (Exception ex) {
      System.err.println("A random exception occurred:\n" + ex.getMessage());
    }
  }

}
