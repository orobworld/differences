package difflist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Problem holds the details of the problem instance.
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  private final Set<Integer> diffs;      // the set of absolute differences
  private final int lb;                  // lower bound on original values
  private final int ub;                  // upper bound on original values
  private final HashSet<Pair> tabu;      // pairs that are not allowed
  private final HashMap<Integer, HashSet<Pair>> coverSet;
                                         // pairs whose difference matches
                                         // a target difference
  private final ArrayList<Pair> allPairs;  // list of all pairs

  /**
   * Constructor.
   * @param dlist the pairwise absolute differences (duplicates removed)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  Problem(final int[] dlist) {
    diffs = Arrays.stream(dlist).boxed().collect(Collectors.toSet());
    // Set the bounds on the source list.
    lb = 1;
    ub = 1 + diffs.stream().mapToInt((i) -> i).max().getAsInt();
    // Create collections of integer pairs that do or do not produce an
    // absolute difference in the target list.
    tabu = new HashSet<>();
    coverSet = new HashMap<>();
    diffs.forEach((d) -> {
      coverSet.put(d, new HashSet<>());
    });
    allPairs = new ArrayList<>();
    for (int i = lb; i < ub; i++) {
      for (int j = i + 1; j <= ub; j++) {
        Pair p = new Pair(i, j);
        allPairs.add(p);
        int d = p.getDiff();
        if (diffs.contains(d)) {
          coverSet.get(d).add(p);
        } else {
          tabu.add(p);
        }
      }
    }
  }

  /**
   * Gets the lower bound on list members.
   * @return the lower bound
   */
  public int getLb() {
    return lb;
  }

  /**
   * Gets the upper bound on list members.
   * @return the upper bound
   */
  public int getUb() {
    return ub;
  }

  /**
   * Gets the tabu pairs.
   * @return the set of tabu pairs
   */
  public Collection<Pair> getTabuPairs() {
    return Collections.unmodifiableCollection(tabu);
  }

  /**
   * Gets all possible pairs.
   * @return the collection of all possible pairs
   */
  public Collection<Pair> getAllPairs() {
    return Collections.unmodifiableCollection(allPairs);
  }

  /**
   * Tests if a pair is tabu.
   * @param p the pair
   * @return true if the pair is forbidden
   */
  public boolean isTabu(final Pair p) {
    return tabu.contains(p);
  }

  /**
   * Gets the collection of differences needing to be found.
   * @return the collection of differences
   */
  public Collection<Integer> getDiffs() {
    return Collections.unmodifiableCollection(diffs);
  }

  /**
   * Gets the pairs that cover a particular difference.
   * @param difference the difference to cover
   * @return the pairs that cover it
   */
  public Collection<Pair> getCover(final int difference) {
    return Collections.unmodifiableCollection(coverSet.get(difference));
  }
}
